//
//  TradeViewCell.swift
//  BitTicker
//
//  Created by Ahmad Shahrouj on 3/6/19.
//  Copyright © 2019 Ahmad Shahrouj. All rights reserved.
//

import UIKit

class TradeViewCell: UITableViewCell {

    @IBOutlet weak var tradeLabel: UILabel!

    var viewModel: Trade? {
        didSet {
            bindViewModel()
        }
    }
    
    private func bindViewModel() {
        if let viewModel = viewModel {
            tradeLabel?.text = "CurrencyID \(viewModel.currencyId) with LastTrade \(viewModel.lastTrade)"
        }
    }
    
}
