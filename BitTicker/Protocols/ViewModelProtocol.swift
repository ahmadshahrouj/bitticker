//
//  ViewModelProtocol.swift
//  BitTicker
//
//  Created by Ahmad Shahrouj on 3/3/19.
//  Copyright © 2019 Ahmad Shahrouj. All rights reserved.
//

import RxSwift
import RxCocoa

public protocol LoginProtocol {
    var username: BehaviorRelay<String> { get }
    var password: BehaviorRelay<String> { get }
    var submitButtonTapped: PublishSubject<Void> { get }
    var submitButtonEnabled: Observable<Bool> { get }
    var onShowLoadingHud: Observable<Bool> { get }
    var onShowError: PublishSubject<String>  { get }
}

public protocol RegistertionProtocol {
    var fullname: BehaviorRelay<String> { get }
    var username: BehaviorRelay<String> { get }
    var password: BehaviorRelay<String> { get }
    var confirmPassword: BehaviorRelay<String> { get }
    var submitButtonTapped: PublishSubject<Void> { get }
    var submitButtonEnabled: Observable<Bool> { get }
    var onShowLoadingHud: Observable<Bool> { get }
    var onShowError: PublishSubject<String>  { get }
}


public protocol ValidationProtocol {
    func validateLength(text : String, size : (min : Int, max : Int)) -> Bool
    func validatePattern(text : String) -> Bool
    func validate(with password : String, and confirmPassword: String) -> Bool
}

extension ValidationProtocol {
    func validateLength(text : String, size : (min : Int, max : Int)) -> Bool { return true }
    func validatePattern(text : String) -> Bool { return true }
    func validate(with password : String, and confirmPassword: String) -> Bool { return true }
}
