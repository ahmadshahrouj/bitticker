//
//  TradeViewModel.swift
//  BitTicker
//
//  Created by Ahmad Shahrouj on 3/6/19.
//  Copyright © 2019 Ahmad Shahrouj. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum TradeTableViewCellType {
    case normal(cell: Trade)
    case error(message: String)
    case empty
}


class TradeTableViewViewModel {

    var tradeCells: Observable<[TradeTableViewCellType]> {
        return cells.asObservable()
    }
    
    let onShowError = PublishSubject<String>()
    let appServerClient: AppServerClient
    let disposeBag = DisposeBag()
    
    private let loadInProgress = BehaviorRelay(value: false)
    private let cells = BehaviorRelay<[TradeTableViewCellType]>(value: [])
    
    
    private var tradeArray = BehaviorRelay<[Trade]>(value: [Trade.init(currencyId: 0, lastTrade: "")])

    init(appServerClient: AppServerClient = AppServerClient()) {
        self.appServerClient = appServerClient
        
        tradeArray.asObservable().subscribe(onNext: {
            updatedArray in
            
            guard updatedArray.count > 0 else {
                self.cells.accept([.empty])
                return
            }

            self.cells.accept(updatedArray.compactMap { .normal(cell: Trade.init(currencyId: $0.currencyId, lastTrade: $0.lastTrade)) })
        }).disposed(by: disposeBag)
    }
    
    internal func poloniexLive() {
        Poloniex.Live.Subscribe(channel: .ticker, delegate: onTicker)
    }
    
    internal func closePoloniexLive() {
         Poloniex.Live.close()
    }
    
    private func onTicker(message: String) {
        
        guard let trade = setupTheTrade(with: message) else { return }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            if self.tradeArray.value.filter({$0.currencyId == trade.currencyId}).count != 0 {
                self.tradeArray.accept(self.tradeArray.value.filter({$0.currencyId != trade.currencyId}))
                self.tradeArray.accept(self.tradeArray.value + [trade])
                return
            }
            self.tradeArray.accept(self.tradeArray.value + [trade])
        })
    }
    
    private func setupTheTrade(with string: String) -> Trade? {
        let arrayOfStrings = string.components(separatedBy: [Unicode.Scalar(",[") ?? "]",",", "["])
        if arrayOfStrings.count > 4 {
            return Trade.init(currencyId: Int(arrayOfStrings[4]) ?? 1, lastTrade: arrayOfStrings[5])
        }
        return nil
    }
    
}
