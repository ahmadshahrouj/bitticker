//
//  RegistertionViewModel.swift
//  BitTicker
//
//  Created by Ahmad Shahrouj on 3/4/19.
//  Copyright © 2019 Ahmad Shahrouj. All rights reserved.
//

import RxSwift
import RxCocoa

class RegistertionViewModel: RegistertionProtocol, ValidationProtocol {
    
    var fullname = BehaviorRelay<String>(value: "")
    var username = BehaviorRelay<String>(value: "")
    var password = BehaviorRelay<String>(value: "")
    var confirmPassword = BehaviorRelay<String>(value: "")
    var submitButtonTapped = PublishSubject<Void>()
    var submitButtonEnabled: Observable<Bool> {
        return Observable.combineLatest(usernameValid, passwordValid, confirmPasswordValid) { $0 && $1 && $2 }
    }
    
    let onShowError = PublishSubject<String>()
    var onShowLoadingHud: Observable<Bool> {
        return loadInProgress
            .asObservable()
            .distinctUntilChanged()
    }
    
    private var usernameValid: Observable<Bool> {
        let usernameValid = username.asObservable().map { $0.count > 0 && self.validatePattern(text: $0) }
        return usernameValid
    }
    
    private var passwordValid: Observable<Bool> {
        return password.asObservable().map { $0.count > 0 &&
            self.validateLength(text: $0, size: (6,15)) &&
            self.validate(with: $0, and: self.confirmPassword.value)
        }
    }
    
    private var confirmPasswordValid: Observable<Bool> {
        return confirmPassword.asObservable().map { self.validate(with: $0, and: self.password.value)
        }
    }
    
    internal func validateLength(text : String, size : (min : Int, max : Int)) -> Bool {
        
        guard (size.min...size.max).contains(text.count) else {
            showError(with: "Validate Length", and: "Please enter a valid Password.")
            return false
        }
        showError(with: "", and: "")
        return true
    }
    
    internal func validatePattern(text : String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        guard emailTest.evaluate(with: text) else {
            showError(with: "Validate Pattern", and: "Please enter a valid Email Id.")
            return false
        }
        showError(with: "", and: "")
        return true
    }
    
    func validate(with password : String, and confirmPassword: String) -> Bool {
        if password != confirmPassword {
            showError(with: "Validate Passwrod", and: "Password doesn't match")
            return false
        }
        showError(with: "", and: "")
        return true
    }
    
    
    private let loadInProgress = BehaviorSubject<Bool>(value: false)
    private let disposeBag = DisposeBag()
    
    private let appServerClient: AppServerClientProtocol!
    init(_ appServerClient: AppServerClientProtocol = AppServerClient()) {
        
        self.appServerClient = appServerClient
        
        submitButtonTapped.subscribe( onNext: { [weak self] in
            self?.registerPressed()
        }).disposed(by: disposeBag)
    }
    
    private func registerPressed() {
        
        let user = User(email: username.value, password: password.value, fullname: username.value)
        self.appServerClient.signUp(with: user).subscribe(
            onNext: { [weak self] _ in
                self?.loadInProgress.onNext(false)
            },
            onError: { [weak self] error in
                self?.loadInProgress.onNext(true)
                self?.showError(with: "API Error", and: "Check your network and try again later.")
        }).disposed(by: disposeBag)
    }
    
    private func showError(with title: String, and message: String) {
        self.onShowError.onNext(message)
    }
}
