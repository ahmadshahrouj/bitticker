//
//  loginViewModel.swift
//  BitTicker
//
//  Created by Ahmad Shahrouj on 3/3/19.
//  Copyright © 2019 Ahmad Shahrouj. All rights reserved.
//

import RxSwift
import RxCocoa

class loginViewModel: LoginProtocol, ValidationProtocol {
   
    var username = BehaviorRelay<String>(value: "")
    var password = BehaviorRelay<String>(value: "")
    var submitButtonTapped = PublishSubject<Void>()
    var submitButtonEnabled: Observable<Bool> {
        return Observable.combineLatest(usernameValid, passwordValid) { $0 && $1 }
    }
    
    let onShowError = PublishSubject<String>()
    var onShowLoadingHud: Observable<Bool> {
        return loadInProgress
            .asObservable()
            .distinctUntilChanged()
    }
    
    private var usernameValid: Observable<Bool> {
        let usernameValid = username.asObservable().map { $0.count > 0 && self.validatePattern(text: $0) }
        return usernameValid
    }
    private var passwordValid: Observable<Bool> {
        return password.asObservable().map { $0.count > 0 && self.validateLength(text: $0, size: (6,15)) }
    }
    
    internal func validateLength(text : String, size : (min : Int, max : Int)) -> Bool {
        
        guard (size.min...size.max).contains(text.count) else {
            showError(with: "Validate Length", and: "Please enter a valid Password.")
            return false
        }
        showError(with: "", and: "")
        return true
    }
    
    internal func validatePattern(text : String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        guard emailTest.evaluate(with: text) else {
            showError(with: "Validate Pattern", and: "Please enter a valid Email Id.")
            return false
        }
        showError(with: "", and: "")
        return true
    }
    
    private let loadInProgress = BehaviorSubject<Bool>(value: false)
    private let disposeBag = DisposeBag()
   
    private let appServerClient: AppServerClientProtocol!
    init(_ appServerClient: AppServerClientProtocol = AppServerClient()) {
      
        self.appServerClient = appServerClient

        submitButtonTapped.subscribe( onNext: { [weak self] in
                    self?.loginPressed()
                }).disposed(by: disposeBag)
    }
    
    private func loginPressed() {
        self.appServerClient.signIn(with: username.value, and: password.value).subscribe(
                onNext: { [weak self] _ in
                    self?.loadInProgress.onNext(false)
                },
                onError: { [weak self] error in
                    self?.loadInProgress.onNext(true)
                    self?.showError(with: "API Error", and: "Check your network and try again later.")
                }).disposed(by: disposeBag)
    }
    
    private func showError(with title: String, and message: String) {
        self.onShowError.onNext(message)
    }
}
