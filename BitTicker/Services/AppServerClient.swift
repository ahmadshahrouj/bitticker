//
//  LoginService.swift
//  BitTicker
//
//  Created by Ahmad Shahrouj on 3/3/19.
//  Copyright © 2019 Ahmad Shahrouj. All rights reserved.
//

import Foundation
import RxSwift

protocol AppServerClientProtocol {
    func signIn(with username: String, and passwrod: String) -> Observable<Void>
    func signUp(with user: User) -> Observable<Void>
}

class AppServerClient: AppServerClientProtocol {
    
    func signIn(with username: String, and passwrod: String) -> Observable<Void> {
        
        let param = ["username": username,
                     "passwrod": passwrod]
        
        return Observable<Void>.create { [param] observer -> Disposable in
            // case .success observer.onNext()
            // case .failure(let error)
            return Disposables.create()
        }
    }
    
    func signUp(with user: User) -> Observable<Void> {
        
        let param = ["username": user.email,
                     "fullname": user.fullname,
                     "passwrod": user.password]
        
        return Observable<Void>.create { [param] observer -> Disposable in
            // case .success observer.onNext()
            // case .failure(let error)
            return Disposables.create()
        }
    }
}
