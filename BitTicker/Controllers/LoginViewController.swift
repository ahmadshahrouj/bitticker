//
//  LoginViewController.swift
//  BitTicker
//
//  Created by Ahmad Shahrouj on 3/3/19.
//  Copyright © 2019 Ahmad Shahrouj. All rights reserved.
//

import UIKit


class LoginViewController: BaseViewController {
   
    // MARK: - Properties
    private var viewModel = loginViewModel()
    
    // MARK: - UI
    @IBOutlet weak var passwordTextfield: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        configure(with: viewModel)
    }
    
    func configure(with viewModel: loginViewModel) {
       
        super.bind(textField: emailTextfield, to: viewModel.username)
        super.bind(textField: passwordTextfield, to: viewModel.password)
        
        viewModel.submitButtonEnabled
            .bind(to: signInButton.rx.isEnabled)
            .disposed(by: disposeBag)
        
        signInButton.rx.tap.asObservable()
            .bind(to: viewModel.submitButtonTapped)
            .disposed(by: disposeBag)
        
        viewModel.onShowError
            .map { [weak self] in
                self?.errorLabel.isHidden = $0.isEmpty
                self?.errorLabel.text = $0
            }.subscribe()
            .disposed(by: disposeBag)
    }
}
