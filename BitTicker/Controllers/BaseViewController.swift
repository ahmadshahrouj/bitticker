//
//  BaseViewController.swift
//  BitTicker
//
//  Created by Ahmad Shahrouj on 3/4/19.
//  Copyright © 2019 Ahmad Shahrouj. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxSwiftExt

class BaseViewController: UIViewController {
   
    internal let disposeBag = DisposeBag()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    internal func bind(textField: UITextField, to behaviorRelay: BehaviorRelay<String>) {
        behaviorRelay.asObservable()
            .bind(to: textField.rx.text)
            .disposed(by: disposeBag)
        textField.rx.text.unwrap()
            .bind(to: behaviorRelay)
            .disposed(by: disposeBag)
    }
}
