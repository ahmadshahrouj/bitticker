//
//  TradeViewController.swift
//  BitTicker
//
//  Created by Ahmad Shahrouj on 3/4/19.
//  Copyright © 2019 Ahmad Shahrouj. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class TradeViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    let viewModel: TradeTableViewViewModel = TradeTableViewViewModel()
    private let disposeBag = DisposeBag()

    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let nibCell = UINib(nibName: "TradeViewCell", bundle: nil)
        tableView.register(nibCell, forCellReuseIdentifier: "TradeViewCell")
        
        bindViewModel()
        viewModel.poloniexLive()
    }
    
    func bindViewModel() {
        viewModel.tradeCells.bind(to: self.tableView.rx.items) { tableView, index, element in
            let indexPath = IndexPath(item: index, section: 0)
            switch element {
            case .normal(let viewModel):
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "TradeViewCell", for: indexPath) as? TradeViewCell else {
                    return UITableViewCell()
                }
                cell.viewModel = viewModel
                return cell
            case .error(let message):
                let cell = UITableViewCell()
                cell.isUserInteractionEnabled = false
                cell.textLabel?.text = message
                return cell
            case .empty:
                let cell = UITableViewCell()
                cell.isUserInteractionEnabled = false
                cell.textLabel?.text = "No data available"
                return cell
            }
            }.disposed(by: disposeBag)
        
        viewModel
            .onShowError
            .map { print("error \($0)")}
            .subscribe()
            .disposed(by: disposeBag)
    }
    
    deinit {
        viewModel.closePoloniexLive()
    }
}
