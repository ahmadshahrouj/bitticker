//
//  Models.swift
//  BitTicker
//
//  Created by Ahmad Shahrouj on 3/3/19.
//  Copyright © 2019 Ahmad Shahrouj. All rights reserved.
//

import Foundation

struct User: Codable {
    let email: String
    let password: String
    let fullname: String
    
    init(email: String, password: String, fullname: String) {
        self.fullname = fullname
        self.password = password
        self.email = email
    }
}

struct Trade: Codable {
    let currencyId: Int
    var lastTrade: String
    
    init(currencyId: Int, lastTrade: String) {
        self.currencyId = currencyId
        self.lastTrade = lastTrade
    }
}
